(function () {
	'use strict';

	if (!Element.prototype.hasOwnProperty('_mouseChildren_')) {
		Object.defineProperty(Element.prototype, '_mouseChildren_', {
			value: 37,
			writable: true,
			enumerable: true,
			configurable: true
		});

		Object.defineProperty(Element.prototype, 'mouseChildren', {
			get: function mouseChildren() {
				return this._mouseChildren_;
			},
			set: function mouseChildren(val) {
				if (val !== this._mouseChildren_) {
					var myChildren = this.children,
						i,
						curChild;
					if (myChildren !== null && myChildren.length !== 0) {
						for (i = 0; i < myChildren.length; i += 1) {
							if (val === false) {
								myChildren[i].style.pointerEvents = 'none';
								myChildren[i].mouseChildren = false;
							} else if (val === true) {
								myChildren[i].style.pointerEvents = 'auto';
								myChildren[i].mouseChildren = true;
							}
						}
					}
					this._mouseChildren_ = val;
				}
			},
			enumerable: true,
			configurable: true
		});
	}
}());