(function () {
	'use strict';
	var states = {
			normal : {
				ctx : null,
				onLeave : function (cell) {},
				onEnter : function (cell) {
					cell = null;
				},
				select : function (cell) {
					this.ctx.moveToState(this.ctx.states.cellSelected, cell);
				},
				edit : function (cell) {
					this.ctx.moveToState(this.ctx.states.cellEditing, cell);
				}
			},
			cellSelected : {
				notes : null,
				ctx : null,
				preventHiddingComments : false,
				editListener: null,
				comments: null,
				onLeave : function (cell) {
					if (!this.preventHiddingComments) {
						this.ctx.selectedCell.mouseChildren = false;
						this.ctx.selectedCell.classList.remove('selected');
						this.ctx.selectedCell = null;

						if (this.notes !== null) {
							this.notes.classList.add('hidden');
							this.notes = null;
						}
					}
					
					if(this.comments !== null) {
						// Remove the listener to the edit comments event
						this.comments.removeEventListener('click', this.editListener);
					} 
				},
				onEnter : function (cell) {
					var comments;
					if (this.preventHiddingComments) {
						this.preventHiddingComments = false;
					}

					this.ctx.selectedCell = cell;
					cell.classList.add('selected');

					this.notes = cell.querySelector('.notes');
					if (this.notes !== null) {
						this.notes.classList.remove('hidden');
					}

					cell.mouseChildren = true;

					
					this.comments = cell.querySelector('.notes .text');
					if(this.comments !== null) {
						// Add the listener to the edit comments event
						this.editListener = this.onEditCommentsEvent.bind(this);
						this.comments.addEventListener('click', this.editListener);
					}
				},
				select : function (cell) {
					if (this.ctx.selectedCell === cell ||
						this.ctx.selectedCell === cell.parentElement) {
						this.ctx.moveToState(this.ctx.states.normal, cell);
					} else {
						this.ctx.moveToState(this.ctx.states.cellSelected, cell);
					}
				},
				edit : function (cell) {
					if(this.ctx.selectedCell === cell.parentElement){
						cell = cell.parentElement;
					}
					this.ctx.moveToState(this.ctx.states.cellEditing, cell);
				},
				onEditCommentsEvent : function (e) {
					
					e.currentTarget.removeEventListener('click', this.editListener);

					this.preventHiddingComments = true;
					this.ctx.moveToState(this.ctx.states.commentsEditing, e.currentTarget);
				}
			},
			cellEditing : {
				ctx : null,
				parent : null,
				changeListener : null,
				inputField : null,
				editedValueCell : null, // when a number cell, it is diferent than ctx.editedCell
				onLeave : function (cell) {
					var newValue,
						actIndex,
						students,
						rowCells,
						actId,
						i;

					// VALIDATION
					// 
					if (this.inputField.classList.contains('act') || this.inputField.classList.contains('st_name')) {
						if(this.inputField.value === '') {
							newValue = 'SIN NOMBRE';
						} else {
							newValue = this.inputField.value;
						}
					} else if (this.inputField.classList.contains('number')) {
						if (this.inputField.value === '' || typeof this.inputField.value === 'number') {
							newValue = '#'; 
						} else {
							newValue = this.inputField.value;
						}
					} else {
						newValue = this.inputField.value;
					}

					this.editedValueCell.textContent = newValue;

					// Update comments depending on wheher the edited cell was an activity name or a studentes name.
					if (this.inputField.classList.contains('act')) {
						// get the index of the edited activity name
						
						actId = this.ctx.editedCell.getAttribute('data-col');
						actIndex = parseInt(actId.split('A')[1]);/*Array.prototype.
								   indexOf.call(this.inputField.parentElement.children, this.inputField) + 1;*/
						// get all student rows
						students = document.querySelectorAll('.alumno');
						// change the activityname in the comments of the right column
						for (i = 0; i < students.length; i += 1) {
							students[i].children[actIndex].
								querySelector('.actName').textContent = newValue;//								this.inputField.value;
						}
					} else if (this.inputField.classList.contains('st_name')) {
						// get all current row cells
						rowCells = this.inputField.parentElement.children;
						console.log(rowCells);
						// change comments in all of its cells except the first one, which is the student name.
						for (i = 1; i < rowCells.length -1; i += 1) {
							rowCells[i].querySelector('.stName').textContent = newValue;//								this.inputField.value;
						}
					}
					// now we replace the editeble input text with the editedCell with the updated content
					this.parent.replaceChild(this.editedValueCell, this.inputField);

					if (this.ctx.editedCell.hasAttribute('data-formula')) {
						this.ctx.editedCell.setAttribute('data-formula', newValue);
						this.ctx.updateFormulas();
					}
					this.editedValueCell = null;
					this.ctx.editedCell = null;
					this.inputField = null;

					SaveToLocalStorage();
					
				},
				onEnter : function (cell) {
					var i;
					this.ctx.editedCell = cell;

					this.inputField = document.createElement('input');
					for (i = 0; i < cell.classList.length ; i += 1) {
						this.inputField.classList.add(cell.classList[i]);
					}
					this.inputField.classList.add('editing');
					

					if (cell.classList.contains('number')) {
						// number labels are firsChild inside a div
						this.editedValueCell = cell.firstElementChild;
					} 
					else {
						this.editedValueCell = cell;
					}
					this.inputField.value = this.editedValueCell.textContent;

					this.parent = this.editedValueCell.parentNode;
					this.parent.replaceChild(this.inputField, this.editedValueCell);
					this.inputField.focus();
					this.changeListener = this.onChangeEvent.bind(this);
					this.inputField.addEventListener('keydown', this.changeListener);
				},
				select : function (cell) {
					if (!cell.classList.contains('editing')) {
						this.ctx.moveToState(this.ctx.states.cellSelected, cell);
					}
				},
				edit : function (cell) {
					this.ctx.moveToState(this.ctx.states.cellEditing, cell);
				},
				onChangeEvent : function (e) {
					if(e.keyCode === 13 || e.keyCode === 9) {
						e.target.removeEventListener('keydown', this.changeListener);
						this.ctx.moveToState(this.ctx.states.normal);
					}
				}
			},
			commentsEditing : {
				ctx : null,
				CKEinstance : null,
				doneListener : null,
				doneBtn : null,
				editedComment : null,
				normalClassList : [],
				ignoreTimer : null,
				onLeave : function (cell) {
					var i;
					console.log('\n\t--> leaving comment editing mode. ###');
					
					this.destroyDoneButton();
					this.doneBtn = null;
					this.editedComment.setAttribute('contenteditable', 'false');
					
					this.CKEinstance.destroy();
					this.CKEinstance = null;

					for (i = 0; i < this.editedComment.length ; i += 1) {
						this.editedComment.classList.remove(this.editedComment.classList[0]);
					}
					for (i = 0; i < this.normalClassList.length ; i += 1) {
						this.editedComment.classList.add(this.normalClassList[i]);
					}
					this.normalClassList = [];
					this.editedComment = null;

					this.ctx.selectedCell.classList.remove('cell_comm_edit');
					this.ctx.selectedCell.classList.remove('selected');
					eventProcessor.activate();
					SaveToLocalStorage();
				},
				onEnter : function (cell) {
					var i;
					console.log('Entering comment editing mode');
					eventProcessor.deactivate();
					this.editedComment = cell;
					// store cell classes to do a reset on when leaving
					for (i = 0; i < cell.classList.length ; i += 1) {
						this.normalClassList[i] = cell.classList[i];
					}
					this.ctx.selectedCell.classList.add('cell_comm_edit');

					cell.setAttribute('contenteditable', 'true');
					this.CKEinstance = CKEDITOR.inline(cell, {
						tabSpaces : '4'
					});
					this.createDoneButton(cell.parentElement);
				},
				select : function (cell) { },
				edit : function (cell) { },
				stopEdit : function (cell) {
					this.ctx.moveToState(this.ctx.states.cellSelected, e.currentTarget);
				},
				createDoneButton : function (where) {
					var doneBtnHolder = where.
									appendChild(document.createElement('div'));

					doneBtnHolder.classList.add('done_btn');
					this.doneBtn = doneBtnHolder.appendChild(document.createElement('button'));
					this.doneBtn.setAttribute('type', 'button');
					this.doneBtn.textContent = "Done";
					this.doneListener =  this.onDoneClick.bind(this);
					this.doneBtn.addEventListener('click', this.doneListener, true);

				},
				destroyDoneButton : function () {
					this.editedComment.parentElement.removeChild(this.doneBtn.parentElement);
					this.doneBtn = null;
				},
				onDoneClick : function (e) {
					e.stopPropagation();
					this.doneBtn.removeEventListener('click', this.doneListener, true);
					this.ctx.moveToState(this.ctx.states.cellSelected, this.ctx.selectedCell);
				}
			}
		},
		stateMachine = {
			states : null,
			state : null,
			selectedCell : null,
			editedCell : null,
			calc: null,
			/**
			 * Asign the states object and the default state
			 * @param  {Object} myStates     An object containing all possible states as objects again
			 * @param  {Object} defaultState Default state o
			 */
			init : function (myStates, defaultState) {
				this.state = defaultState;
				this.states = myStates;
				var stateIds = Object.keys(myStates);
				stateIds.forEach(function (el, ind, array) {
					myStates[el].ctx = this;
				}, this);
				this.calc = calculator;
				this.recoverAllCalcCols();
			},
			moveToState: function (newState, cell) {
				this.state.onLeave(cell);
				this.state = newState;
				this.state.onEnter(cell);
			},
			select: function (cell) {
				this.state.select(cell);
			},
			edit: function (cell) {
				this.state.edit(cell);
			},
			updateFormulas: function (colId) {
				if (colId !== null && typeof colId !== 'undefined') {
					this.calc.update(colId);
				} else {
					this.calc.updateAll();
				}
			},
			recoverAllCalcCols : function () {
				var activities = document.getElementById('heading-cells').children,
				i;
				for ( i = 0; i < activities.length; i+=1) {
					if(activities[i].hasAttribute('data-formula')) {
						this.calc.registerColumn(activities[i].getAttribute('data-col'));
					}
				}
			}
		},
		eventProcessor = {
			stateMachine : null,
			cells : null,
			preventSingleClick : false,
			clickDelay : 200,
			timer : null,

			addActBtn : null,
			addStudentBtn : null,
			addCalcColBtn : null,
			saveAsBtn : null,
			
			clickListener : null,
			dbClickListener : null,

			addStListener : null,
			addActListener : null,
			addCalcColListener : null,

			contentId : 'content' ,

			init : function init(fsm) {
				var i;
				this.stateMachine = fsm;
				this.cells = document.querySelectorAll('.editable');
				this.addActBtn = document.getElementById('addActivity');
				this.addStudentBtn = document.getElementById('addStudent');
				this.addCalcColBtn = document.getElementById('addCalcCol');
				this.saveAsBtn =  document.getElementById('save');

				// As bind creates a new fucntions, this is the only way to store a reference to it so the listener 
				// can be removed if needed
				this.clickListener = this.cellClicked.bind(this);
				this.dbClickListener = this.cellDoubleClicked.bind(this);

				this.addStListener = this.onAddStBtnClick.bind(this);
				this.addActListener = this.onAddActBtnClick.bind(this);
				this.addCalcColListener = this.onAddCalcColBtnClick.bind(this);

				this.addActBtn.addEventListener('click', this.addActListener);
				this.addStudentBtn.addEventListener('click', this.addStListener);
				this.addCalcColBtn.addEventListener('click', this.addCalcColListener);

				this.saveAsBtn.addEventListener('click', SaveAs);

				this.activate();

			},
			deactivate : function deactivate() {
				var i;
				for (i = 0; i < this.cells.length; i = i + 1) {
					this.removeCellEvents(this.cells[i]);
				}
			},
			activate : function activate() {
				var i;
				for (i = 0; i < this.cells.length; i = i + 1) {
					this.addCellEvents(this.cells[i]);
				}
			},
			addCellEvents : function addCellEvents(cell) {
				if (cell !== null) {
					cell.mouseChildren = false;
					cell.addEventListener('click', this.clickListener);
					cell.addEventListener('dblclick', this.dbClickListener);
				}
			},
			removeCellEvents : function removeCellEvents(cell) {
				if (cell !== null) {
					//cell.mouseChildren = true;
					cell.removeEventListener('click', this.clickListener);
					cell.removeEventListener('dblclick', this.dbClickListener);
				}
			},
			cellClicked : function cellClicked(e) {
				this.timer = setTimeout( (function onTimeOut() {
					if (!this.preventSingleClick) {
						console.log('clicked ' + e.target.classList);
						this.stateMachine.select(e.target);
					}
					this.preventSingleClick = false;
				}).bind(this), this.clickDelay);
			},
			cellDoubleClicked : function cellDoubleClicked(e) {
				clearTimeout(this.timer);
				this.preventSingleClick = true;
				console.log('cellDoubleClicked ' + e.target.classList);
				this.stateMachine.edit(e.target);
			},
			onAddStBtnClick : function onAddStButtonClick(e) {
				var studentsHolder = document.getElementById('alumnos'),
					activities = document.getElementById('heading-cells').children,
					newStudent,
					newCell,
					content = prompt('Introduce el nombre del alumno'),
					i;

				if (content === null) {
					return;
				}

				newStudent = document.createElement('div');
				newStudent.classList.add('alumno');

				newCell = document.createElement('label');
				newCell.classList.add('cell', 'left', 'editable', 'st_name');
				newCell.textContent = content || 'SIN NOMBRE';
				newStudent.appendChild(document.createTextNode('\n'));
				newStudent.appendChild(newCell);
				newStudent.appendChild(document.createTextNode('\n'));
				this.addCellEvents(newCell);

				for (i = 0; i < activities.length; i += 1) {
					this.appendEmptyNumberCell(newStudent, newCell.textContent, activities[i].textContent, activities[i].getAttribute('data-col'), activities[i].getAttribute('data-formula'));
				}
				studentsHolder.appendChild(newStudent);

				this.cells = document.querySelectorAll('.editable');

				SaveToLocalStorage();
			},
			onAddActBtnClick : function onAddActButtonClick(e) {
				var students = document.querySelectorAll('.alumno'),
					activities = document.getElementById('heading-cells').children,
					actsCount = activities.length,
					studentCount = students.length,
					newActivity = document.createElement('label'),
					heading = document.getElementById('heading-cells'),
					i,
					stdName,
					content = prompt('Introduce el nombre de la nueva actividad');
				
				if (content === null) {
					return;
				}

				newActivity.classList.add('cell', 'act', 'editable');
				newActivity.setAttribute('id','A' + (actsCount + 1));
				newActivity.setAttribute('data-col','A' + (actsCount + 1));
				newActivity.textContent = content || 'Actividad ' + (actsCount + 1);
				heading.appendChild(document.createTextNode('\n'));
				heading.appendChild(newActivity);
				this.addCellEvents(newActivity);

				for (i = 0; i < studentCount; i += 1) {
					stdName = students[i].firstElementChild.textContent;
					this.appendEmptyNumberCell(students[i], stdName, newActivity.textContent, 'A' + (actsCount + 1));
				}

				this.cells = document.querySelectorAll('.editable');

				SaveToLocalStorage();
			},
			appendEmptyNumberCell : 
			function appendEmptyNumberCell(holder, studentName, actName, dataId, dataFormula) {
				var newCell = document.createElement('div');
				newCell.classList.add('cell', 'number', 'editable');
				newCell.setAttribute('data-col', dataId);
				if (dataFormula !== null){
					newCell.setAttribute('data-formula', dataFormula);
				}
				newCell.innerHTML = 	
					'\n<div class="val">#</div>\n' +  			
					'<div class="block notes selected hidden">\n' +  						
						'<h2 class="block stName">' + studentName + '</h2>\n' + 
						'<h3 class="block actName">' + actName + '</h3>\n' +
						'<div class="block text">\n' +
							'<p>No hay comentarios todavía</p>\n' +
						'</div>\n' +
					'</div>\n';
				holder.appendChild(newCell);
				// TODO: Find the way to avoid this 'new line' text node
				holder.appendChild(document.createTextNode('\n'));
				this.addCellEvents(newCell);
				return newCell;
			},
			onAddCalcColBtnClick : function onAddActButtonClick(e) {
				var students = document.querySelectorAll('.alumno'),
					studentCount = students.length,
					activities = document.getElementById('heading-cells').children,
					actsCount = activities.length,
					newActivity = document.createElement('label'),
					heading = document.getElementById('heading-cells'),
					i,
					stdName,
					calcCell,
					calcCells = [],
					content = prompt('Introduce la fórmula de cálculo\nA1 se refiere a la primera columna/actividad, A2 a la segunda,...');

				if (content === null) {
					return;
				}

				newActivity.classList.add('cell', 'act', 'editable', 'calculated');
				newActivity.setAttribute('id','A' + (actsCount + 1));
				newActivity.setAttribute('data-col','A' + (actsCount + 1));
				newActivity.setAttribute('data-formula', content);
				newActivity.textContent = content;
				heading.appendChild(document.createTextNode('\n'));
				heading.appendChild(newActivity);
				this.addCellEvents(newActivity);

				for (i = 0; i < studentCount; i += 1) {
					stdName = students[i].firstElementChild.textContent;
					calcCell = this.appendEmptyNumberCell(
						students[i], 
						stdName, 
						newActivity.textContent,
						'A' + (actsCount + 1)
						);
					calcCell.classList.add('calculated');
					calcCells.push(calcCell);
				}
				//Update cells array (events related)
				this.cells = document.querySelectorAll('.editable');

				this.stateMachine.calc.registerColumn('A' + (actsCount + 1));
				this.stateMachine.calc.update('A' + (actsCount + 1));
				SaveToLocalStorage();
			}
		},
		calculator = {
			calcColumns : [],
			registerColumn : function registerColumn(id) {
				this.calcColumns[id.split('A')[1]] = id;
			},
			calculateRow : function calculateRow(row, formulaId, students) {
				var i,
					actFormula = document.getElementById(formulaId),
					stringFormula = actFormula.getAttribute('data-formula').toUpperCase(),
					formula = Parser.parse(stringFormula),
					varCols = formula.variables(),
					varObj = {};
				
				students = students || document.querySelectorAll('.alumno');

				for (i = 0; i < varCols.length; i++) {
					varObj[varCols[i]] = Number (
							students[row].
								querySelector('[data-col="' + varCols[i] + '"] .val').
								textContent
							);
				}
				return parseFloat((Math.round( formula.evaluate(varObj) * 10 ) / 10).toFixed(1));

			},
			update : function update(colId) {
				var students = document.querySelectorAll('.alumno'),
					calcCells = document.querySelectorAll('[data-col="' + colId + '"]'),
					j;
 
				for (j=0; j < students.length; j++) {
					// Start on 1 as the first one is the activity name 
					students[j].querySelector('[data-col="' + colId + '"] .val').textContent = this.calculateRow(j, colId,students);
				}
			},
			updateAll : function updateAll() {
				var i,
				colId;
				for (i = 0; i < this.calcColumns.length; i++) {
					if(this.calcColumns[i] !== null && typeof this.calcColumns[i] !== 'undefined') {
						this.update(this.calcColumns[i]);
					}
				}
			}
		};

	function SaveToLocalStorage() {
		/*
		
		TODO: Store date to only use it if local file date is earlier.
		
		var entryName = 'CAL_' + getDocFileName().split('.')[0];
		entryObject = {
			content:(new XMLSerializer).serializeToString(document.getElementById('content'));
			date:
		}*/

		window.localStorage.setItem( getDocFileName().split('.')[0]+ '_content', document.getElementById('content').outerHTML);
	}

	function SaveAs() {
		var fileURL = document.URL;
		// remove style element, CKEDITOR creates one.
		var styleElement = document.querySelector('style');
		if(styleElement !== null && styleElement !== undefined )
		styleElement.parentElement.removeChild(styleElement);
		SaveToDisk(getDocFileName(fileURL));
	}

	function getDocFileName(url) {
		var fileURL = url || document.URL;
		var splitedURL = document.URL.split('/');
		return splitedURL[splitedURL.length - 1];
	}

	function SaveToDisk(fileName) {
		console.log('<!DOCTYPE html>\n' + document.querySelector('html').outerHTML);
		var blobData = ['<!DOCTYPE html>\n' + document.querySelector('html').outerHTML];
		var toSaveBlob = new Blob(blobData, {type : 'text/html'});

	    var reader = new FileReader();
	    reader.readAsDataURL(toSaveBlob);

	    reader.onload = function(event) {
	    	var save = document.createElement('a');
	        save.href = event.target.result;
	        save.target = '_blank';
	        save.download = fileName || 'unknown';

	        var event = document.createEvent('Event');
	        event.initEvent('click', true, true);
	        save.dispatchEvent(event);
	        (window.URL || window.webkitURL).revokeObjectURL(save.href);
	    };
	}

	function start() {
	
		stateMachine.init(states, states.normal);
		eventProcessor.init(stateMachine);

		CKEDITOR.disableAutoInline = true;
		CKEDITOR.dtd.$editable.span = 1;

	}

	function checkCache() {
		var stored = window.localStorage.getItem(getDocFileName().split('.')[0] + '_content');
		//console.log(getDocFileName().split('.')[0]);
		if (stored !== null && stored !== undefined) {
			//console.log(stored);

			var contentParent = document.getElementById('content').parentElement;
			contentParent.removeChild(document.getElementById('content'));
			contentParent.appendChild(str2DOMElement(stored));
			//contentParent.insertAdjacentHTML('afterbegin', str2DOMElement(stored));
		}
		start();
	}

	//Dirty hack to create an html element out of a string
	function str2DOMElement (html) {
	    var frame = document.createElement('iframe');
	    frame.style.display = 'none';
	    document.body.appendChild(frame);             
	    frame.contentDocument.open();
	    frame.contentDocument.write(html);
	    frame.contentDocument.close();
	    var el = frame.contentDocument.body.firstChild;
	    document.body.removeChild(frame);
	    return el;
	}


	window.addEventListener('load', checkCache);

}());



/* Old versions of methods

	// Works perfectluy but it's slower and the code looks crapy
	appendEmptyNumberCell : function appendEmptyCell(holder, studentName, actName) {
				var newCell = document.createElement('div'),
					newComment = document.createElement('div'),
					newCommentTitle = document.createElement('h2'),
					newCommentAct = document.createElement('h3'),
					newCommentText = document.createElement('div');
				newCell.classList.add('cell', 'number', 'editable');
				newCell.innerHTML = '<div class="val">#</div>';
				newComment.classList.add('block', 'notes', 'selected', 'hidden');

				newCommentTitle.classList.add('block', 'stName');
				if (studentName !== null) {
					newCommentTitle.innerHTML = studentName;
				}

				newCommentAct.classList.add('block', 'actName');
				if (actName !== null) {
					newCommentAct.innerHTML = actName;
				}

				newCommentText.classList.add('block', 'text');
				newCommentText.innerHTML = '<p>No hay comentarios todavía</p>';

				newComment.appendChild(newCommentTitle);
				newComment.appendChild(newCommentAct);
				newComment.appendChild(newCommentText);
				newCell.appendChild(newComment);
				

				holder.appendChild(document.createTextNode('\n'));
				holder.appendChild(newCell);

				this.addCellEvents(newCell);
			},
	

*/