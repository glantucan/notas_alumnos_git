(function () {
	'use strict';
	var editing = false,
		i,
		inEdition,
		selected,
		tinymceActive = false,
		CKEinstance = null;
	/**
	 * Stop editing if the clicked outside the inEdition element
	 * @param {Type} e
	 */
	function onEditFinished(e) {
		if (!tinymceActive) {

			if (e.type === 'change' || e.target !== inEdition) {
				var parent = inEdition.parentNode,
					finishedCell,
					i,
					actIndex,
					students,
					cells;

				editing = false;
				document.removeEventListener('click', onEditFinished);
				document.removeEventListener('change', onEditFinished);

				if (inEdition.classList.contains('number')) {

					finishedCell = document.createElement('span');
					finishedCell.classList.add('val');

				} else {
					finishedCell = document.createElement('span');
					for (i = 0; i < inEdition.classList.length; i += 1) {
						finishedCell.classList.add(inEdition.classList[i]);
					}
					if (inEdition.classList.contains('act')) {
						actIndex = Array.prototype.
								   indexOf.call(inEdition.parentElement.children, inEdition) + 1;
						students = document.querySelectorAll('.alumno');
						for (i = 0; i < students.length; i += 1) {
							students[i].children[actIndex].
								querySelector('.actName').textContent = inEdition.value;
						}

					} else if (inEdition.classList.contains('st_name')) {
						cells = parent.children;
						for (i = 1; i < cells.length; i += 1) {
							cells[i].querySelector('.stName').textContent = inEdition.value;
						}
					}
				}
				finishedCell.classList.remove('editing');
				finishedCell.textContent = inEdition.value;
				addCellEvents(finishedCell);

				parent.replaceChild(finishedCell, inEdition);
				inEdition = null;
			}
		} else {
			if (e.target.classList.contains('commit')) {
				tinymceActive = false;
				editing = false;
				document.removeEventListener('click', onEditFinished);
				document.removeEventListener('change', onEditFinished);

				var finishedText = document.createElement('span');
				finishedText.classList.add('block', 'text');
				finishedText.innerHTML = tinyMCE.activeEditor.getContent();
				addCellEvents(finishedText);
				e.target.parentElement.
					replaceChild(finishedText, document.querySelector('.mce-container'));
				var oldText =document.querySelector('.tinyMCE');
				e.target.parentElement.removeChild(document.querySelector('.tinyMCE'));
				e.target.parentElement.removeChild(e.target);

				inEdition = null;

			}
		}
	}

	function cellClicked(e) {
			if (!e.target.classList.contains('text') &&
				!e.target.parentElement.classList.contains('text') ) {
				var prevSelected, notes;

				prevSelected = selected || null;
				if (!editing) {
					if (prevSelected !== e.currentTarget) {
						selected = e.currentTarget;
						selected.classList.add('selected');
						if (selected.classList.contains('number')) {
							notes = selected.querySelector('.notes');
							notes.classList.remove('hidden');
						}
					}

					if (prevSelected !== null) {
						prevSelected.classList.remove('selected');
						if (prevSelected.classList.contains('number')) {
							notes = prevSelected.querySelector('.notes');
							notes.classList.add('hidden');
						}
					}
					if (prevSelected === selected) {
						selected = null;
					}
				}
			} else {

				// TODO: set a submit button


				if(CKEinstance === null) {
					CKEinstance = CKEDITOR.inline( e.target );
				} else if (	!e.target.classList.contains('cke_editable') &&
							!e.target.parentElement.classList.contains('cke_editable') &&
							!e.target.parentElement.parentElement.classList.contains('cke_editable')) {
					CKEinstance.destroy();
					CKEinstance = null;
				}

			}


	}

	function cellDoubleClicked(e) {
		editing = true;
		var clickedCell = e.currentTarget,
			parent,
			editable = document.createElement('input'),
			i;
		if (!clickedCell.classList.contains('number') && !e.target.classList.contains('text')) {
			parent = clickedCell.parentNode;

			for (i = 0; i < clickedCell.classList.length; i += 1) {
				editable.classList.add(clickedCell.classList[i]);
			}
			editable.classList.add('editing');
			editable.classList.remove('selected');
			parent.classList.remove('selected');
			selected = null;

			editable.value = clickedCell.textContent;
			parent.replaceChild(editable, clickedCell);

			inEdition = editable;
			inEdition.focus();
			document.addEventListener('click', onEditFinished);
			inEdition.addEventListener('change', onEditFinished);

		} else if (e.target.classList.contains('text')) {

//			editable = document.createElement('textarea');

//			parent = e.target.parentNode;
//			for (i = 0; i < e.target.classList.length; i += 1) {
//				editable.classList.add(e.target.classList[i]);
//			}
//			editable.classList.add('editing');
//			editable.classList.add('tinyMCE');
//			editable.classList.remove('selected');
//			editable.value = e.target.textContent;
//
//			parent.replaceChild(editable, e.target);
//
			e.target.classList.add('tinyMCE');
//			e.target.classList.add('editing');


			//tinyMCE.activeEditor.getContent();


//			inEdition = editable;
			inEdition = e.target;
			inEdition.focus();
			document.addEventListener('click', onEditFinished);
			inEdition.addEventListener('change', onEditFinished);

			var button = document.createElement('button');
			button.textContent = "commit";
			button.classList.add('commit');
			e.target.parentNode.appendChild(button);

		} else {
			clickedCell = clickedCell.firstElementChild;
			parent = clickedCell.parentNode;

			for (i = 0; i < clickedCell.classList.length; i += 1) {
				editable.classList.add(clickedCell.classList[i]);
			}
			editable.classList.add('number');
			editable.classList.add('editing');
			editable.classList.remove('selected');
			parent.classList.remove('selected');
			selected = null;

			editable.value = clickedCell.textContent;
			parent.replaceChild(editable, clickedCell);

			inEdition = editable;
			inEdition.focus();
			document.addEventListener('click', onEditFinished);
			inEdition.addEventListener('change', onEditFinished);


		}

		//console.log(Array.prototype.slice.call(parent.children).indexOf(clickedCell));
	}



	function addCellEvents(cell) {
		if (cell !== null) {
			cell.addEventListener('mouseup', cellClicked);
			cell.addEventListener('dblclick', cellDoubleClicked);
		}
	}

	function appendEmptyCell(holder, studentName, actName) {
		var newCell = document.createElement('label'),
			newComment = document.createElement('span'),
			newCommentTitle = document.createElement('span'),
			newCommentAct = document.createElement('span'),
			newCommentText = document.createElement('span');
		newCell.classList.add('cell', 'number', 'editable');
		newCell.innerHTML = '<span class="val">-</span>';
		newComment.classList.add('block', 'notes', 'selected', 'hidden');

		newCommentTitle.classList.add('block', 'stName');
		if (studentName !== null) {
			newCommentTitle.innerHTML = studentName;
		}

		newCommentAct.classList.add('block', 'actName');
		if (actName !== null) {
			newCommentAct.innerHTML = actName;
		}

		newCommentText.classList.add('block', 'text');
		newCommentText.innerHTML = 'No hay comentarios todavía';

		newComment.appendChild(newCommentTitle);
		newComment.appendChild(newCommentAct);
		newComment.appendChild(newCommentText);
		newCell.appendChild(newComment);


		holder.appendChild(document.createTextNode('\n'));
		holder.appendChild(newCell);
		addCellEvents(newCell);

	}

	function addActivity(e) {
		var students = document.querySelectorAll('.alumno'),
			studentCount = students.length,
			newActivity,
			newStudentCell,
			curStudent,
			heading = document.getElementById('heading-cells'),
			i,
			stdName;

		newActivity = document.createElement('label');
		newActivity.classList.add('cell', 'act', 'editable');
		newActivity.textContent = 'Actividad ' + heading.children.length;
		heading.appendChild(document.createTextNode('\n'));
		heading.appendChild(newActivity);
		addCellEvents(newActivity);

		for (i = 0; i < studentCount; i += 1) {
			stdName = students[i].firstElementChild.textContent;
			appendEmptyCell(students[i], stdName, newActivity.textContent);
		}
	}


	function addStudent(e) {
		var studentsHolder = document.getElementById('alumnos'),
			activities = document.getElementById('heading-cells').children,
			newStudent,
			newCell,
			i;
		newStudent = document.createElement('div');
		newStudent.classList.add('alumno');

		newCell = document.createElement('label');
		newCell.classList.add('cell', 'left', 'editable', 'st_name');
		newCell.textContent = '-';
		newStudent.appendChild(document.createTextNode('\n'));
		newStudent.appendChild(newCell);
		addCellEvents(newCell);

		for (i = 0; i < activities.length; i += 1) {
			appendEmptyCell(newStudent, '-', activities[i].textContent);
		}
		studentsHolder.appendChild(newStudent);
	}

	function start() {

		var cells = document.querySelectorAll('.editable'),
			addActBtn = document.getElementById('addActivity'),
			addStudentBtn = document.getElementById('addStudent');

		for (i = 0; i < cells.length; i = i + 1) {
			addCellEvents(cells[i]);
		}

		addActBtn.addEventListener('click', addActivity);
		addStudentBtn.addEventListener('click', addStudent);

	}
	CKEDITOR.disableAutoInline = true;
	window.addEventListener('load', start);

}());